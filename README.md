# [Open Science UMontreal][osumontreal]

Git repository for the official website of Open Science UMontreal.

## License

The source code for this site is licensed under [AGPLv3][agpl]. Unless otherwise 
stated, the content of this site is licensed under [CC-BY-SA 4.0][cc].

[agpl]: https://www.gnu.org/licenses/agpl-3.0.txt
[cc]: https://creativecommons.org/licenses/by-sa/4.0/
[osumontreal]: https://umontreal.openscience.ca/
