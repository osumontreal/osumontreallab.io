---
title: "Accueil"
menu: 
  main:
    weight: 1
---

## Notre mission

Open Science UMontréal a pour mission de promouvoir une science au service du 
bien commun, où la société a accès librement aux processus de création, aux 
contenus et retombées de la recherche scientifique en plus de défendre les 
principes d’un monde de recherche plus inclusif, collaboratif et transparent. 

Nous croyons que cette ouverture encouragera une plus grande collaboration nous 
permettant de résoudre plus efficacement les problèmes auxquels nous sommes et 
serons confrontés.

