---
name: "Danny Colin" 
role: "Co-Coordonnateur"
contact: 
  email: "contact@dannycolin.com"
  github: "dannycolin"
  gitlab: "dannycolin"
  twitter: "dannycolincom"
---
